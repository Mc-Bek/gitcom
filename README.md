# About

This is extension for removing marked comments from all files and then return them to files. It will be helpfull for students if they want to commit their projects without their own comments.

## Usage demonstration
![alt text](images/test-ext.gif "Using the extension")
 
## Usage
Mark neccesary comments by concatenating ~ to the default comment symbol of language, such as:
* //~ and /*~ */, 
* #~, 
* %~ 
* etc.

Use ctrl + shift + P to open command line. Then search commands by keyword "GitCom".

Run command "Remove all comments" to remove all marked comments from your files. In your workspace will be create folder .gitcom with file data.json with all information about marked comments.

Run command "Reset all comments" to return all marked comments to your files.
