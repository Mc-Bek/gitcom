import * as fs from 'fs';
import * as vscode from 'vscode';
import { GitComFS } from './GitComFS';

export class Unparse{

    private Unpar(fileUri: vscode.Uri){
        const gitcom = GitComFS.getGitComUri();
        const fileName = gitcom.fsPath + "/data.json";
        var data = JSON.parse(fs.readFileSync(fileName, 'utf8'));
        var i = 0;
        let comres = [];
        while (i < data[fileUri.fsPath].length){
            comres.push(data[fileUri.fsPath][i]);
            i++;            
        }
        return comres;
    }

    public resetComments = (async (fileUri: vscode.Uri, wsEditor: vscode.WorkspaceEdit) => {
            let ml = [];
            let a = this.Unpar(fileUri);
            for (let i = 0; i < a.length; i++){
                let linea = parseInt(a[i]['startLine']);
                let num = a[i]['endLine'] - a[i]['startLine'];
                if(num){
                    ml.push([linea, num]);
                }
                for (let j = 0; j < ml.length; j++){
                    if (linea > ml[j][0]){
                        linea -= ml[j][1];
                    }
                }
                let currPosition: vscode.Position = new vscode.Position(linea, 
                    parseInt(a[i]['startCharacter']));
                wsEditor.insert(fileUri, currPosition, a[i]['com']);
            } 
        }
    );
}