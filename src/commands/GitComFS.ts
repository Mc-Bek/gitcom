import * as vscode from 'vscode';
import * as fs from 'fs';

export class GitComFS {

    private static gitcomUri?: vscode.Uri;

    private static createDirectory(): vscode.Uri{
        let ws = vscode.workspace;
        let wsFolders = ws.workspaceFolders;

        if(wsFolders === undefined) {
            console.log("Workspace folder undefined");
        }
        let gitcomPath = wsFolders!![0].uri.fsPath + "/.gitcom";
        let directoryUri = vscode.Uri.file(gitcomPath);
        ws.fs.createDirectory(directoryUri);
        this.gitcomUri = directoryUri;
        return this.gitcomUri;
    }

    public static getGitComUri(): vscode.Uri{
        if(this.isExists(this.gitcomUri)) {
            return this.gitcomUri!;
        }
        return this.createDirectory();
    }

    public static isExists(fileUri?: vscode.Uri): boolean {
        if(fileUri === undefined) {
            return false;
        }
        return fs.existsSync(fileUri!.fsPath);
    }

    public static async getDataFile() {
        const filePath = this.gitcomUri!.fsPath + "\\data.json";
        const fileUri = vscode.Uri.file(filePath);
        let wsEditor = new vscode.WorkspaceEdit();
        if(!this.isExists(fileUri)){
            wsEditor.createFile(fileUri);
        } else {
            wsEditor.deleteFile(fileUri);
            wsEditor.createFile(fileUri);
        }
        await vscode.workspace.applyEdit(wsEditor);
        return fileUri;
    }
}
